package com.atlassian.jira.plugins.featuredcomments;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.LinkedList;
import java.util.List;

public class EventsManager
{
    List<Event> events = new LinkedList<Event>();

    public void logEvent(final long commentId, final String eventName, final long duration)
    {
        events.add(new Event(commentId, eventName, duration));
    }

    public Iterable<Event> getEvents(final Long commentId)
    {
        return Iterables.filter(events, new Predicate<Event>()
        {
            @Override
            public boolean apply(final Event event)
            {
                return event.commentId == commentId;
            }
        });
    }

    static class Event
    {
        final long commentId;
        final String eventName;
        final long duration;

        Event(final long commentId, final String eventName, final long duration)
        {
            this.commentId = commentId;
            this.eventName = eventName;
            this.duration = duration;
        }
    }
}
