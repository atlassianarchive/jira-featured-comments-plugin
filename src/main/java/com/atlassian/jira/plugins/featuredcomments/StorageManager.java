package com.atlassian.jira.plugins.featuredcomments;

import java.util.HashMap;

public class StorageManager
{
    private final EventsManager eventsManager;

    public StorageManager(final EventsManager eventsManager) {this.eventsManager = eventsManager;}

    public Long getScore(final Long commentId)
    {
        long score = 0;

        HashMap<String, Long> parts = new HashMap<String, Long>();

        for (EventsManager.Event event : eventsManager.getEvents(commentId))
        {
            if (parts.containsKey(event.eventName))
            {
                parts.put(event.eventName, event.duration + parts.get(event.eventName));
            }
            else
            {
                parts.put(event.eventName, event.duration);
            }
        }

        for (String eventName : parts.keySet())
        {
            score += Math.min(parts.get(eventName), 30000);
        }

        return score;
    }
}
