package com.atlassian.jira.plugins.featuredcomments;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.Iterators;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path ("/collect")
@AnonymousAllowed
public class CollectorResource
{
    private final EventsManager eventsManager;

    public CollectorResource(final EventsManager eventsManager) {this.eventsManager = eventsManager;}

    @POST
    @Consumes ({ MediaType.APPLICATION_JSON })
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response publishSingleEvent(final CollectorEventBean bean)
    {
        eventsManager.logEvent(bean.commentId, bean.name, bean.duration);

        return Response.ok().build();
    }

    @Path ("/bulk")
    @POST
    @Consumes ({ MediaType.APPLICATION_JSON })
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response publishSingleEvent(final List<CollectorEventBean> beans)
    {
        for (CollectorEventBean bean : beans)
        {
            eventsManager.logEvent(bean.commentId, bean.name, bean.duration);
        }

        return Response.ok().build();
    }

    static class CollectorEventBean
    {
        @JsonProperty
        String name;

        @JsonProperty
        long commentId;

        @JsonProperty
        long duration;

        CollectorEventBean() { }

        CollectorEventBean(final String name, final long commentId, final long duration)
        {
            this.name = name;
            this.commentId = commentId;
            this.duration = duration;
        }
    }
}
