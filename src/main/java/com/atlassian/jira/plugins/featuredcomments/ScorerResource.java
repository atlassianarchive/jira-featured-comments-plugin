package com.atlassian.jira.plugins.featuredcomments;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Path ("/score")
@AnonymousAllowed
public class ScorerResource
{
    private final CommentManager commentManager;
    private final IssueManager issueManager;
    private final StorageManager storageManager;

    public ScorerResource(final CommentManager commentManager, final IssueManager issueManager, final StorageManager storageManager)
    {
        this.commentManager = commentManager;
        this.issueManager = issueManager;
        this.storageManager = storageManager;
    }

    @GET
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getScore(@QueryParam ("commentId") Long commentId)
    {
        return Response.ok(CommentScoreBean.asBean(commentId, storageManager.getScore(commentId))).build();
    }

    @Path ("/bulk")
    @GET
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getScore(@QueryParam ("commentsIds") List<Long> commentsIds, @QueryParam ("issueKey") String issueKey)
    {
        if (issueKey != null)
        {
            Issue issue = issueManager.getIssueObject(issueKey);
            if (commentsIds == null)
            {
                commentsIds = new ArrayList<Long>();
            }

            commentsIds.addAll(Lists.transform(commentManager.getComments(issue), new Function<Comment, Long>()
            {
                @Override
                public Long apply(final Comment comment)
                {
                    return comment.getId();
                }
            }));
        }

        return Response.ok(Lists.transform(commentsIds, new Function<Long, CommentScoreBean>()
        {
            @Override
            public CommentScoreBean apply(final Long commentId)
            {
                return CommentScoreBean.asBean(commentId, storageManager.getScore(commentId));
            }
        }).toArray()).build();
    }

    @XmlRootElement (name = "comment")
    public static class CommentScoreBean
    {
        @XmlElement
        private Long commentId;

        @XmlElement
        private Long score;

        public CommentScoreBean()
        {
        }

        public CommentScoreBean(final Long commentId, final Long score)
        {
            this.commentId = commentId;
            this.score = score;
        }

        public static CommentScoreBean asBean(final Long commentId, final Long score)
        {
            return new CommentScoreBean(commentId, score);
        }
    }
}
