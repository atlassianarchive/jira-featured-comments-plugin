define('featuredcomments/collector', ['jquery', 'featuredcomments/storage'], function ($, storage)
{
    var eventIdSeq = 0;
    var eventLog = {

    };

    now = function () { return new Date().getTime() };

    return {
        eventStart: function (eventClass, commentId)
        {
            var eventId = eventIdSeq++;

            eventLog[eventId] = {
                eventClass: eventClass,
                commentId: commentId,
                ts: now()
            };

            console.log('Event start:', eventId, eventClass, commentId);

            return eventId;
        },

        eventEnd: function (eventId)
        {
            var event = eventLog[eventId];

            if (!event)
            {
                return;
            }

            console.log('Event end:', eventId, event.eventClass, event.commentId, now() - event.ts);

            storage.pushEvent(event.commentId, event.eventClass, now() - event.ts);
        }
    }
});