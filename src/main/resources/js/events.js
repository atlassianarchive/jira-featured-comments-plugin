define('featuredcomments/events', ['jquery', 'underscore'],
        function ($, _)
        {
            var viewables = [];
            var isUnload = false;

            var syncCheckViewables;
            var checkViewables = _.throttle(syncCheckViewables = function ()
            {
                var scrollTop = $(window).scrollTop();
                var scrollBottom = scrollTop + $(window).height();

                viewables.forEach(function ($viewable)
                {
                    var elementTop = $viewable.offset().top;
                    var elementBottom = elementTop + $viewable.height();
                    var isOutscreen = elementBottom < scrollTop || elementTop > scrollBottom || elementTop == elementBottom || isUnload;

                    if (!(isOutscreen) && !$viewable.prop('viewable'))
                    {
                        $viewable.trigger('viewable');
                        $viewable.prop('viewable', true);
                    }
                    else if (isOutscreen && $viewable.prop('viewable'))
                    {
                        $viewable.trigger('outscreen');
                        $viewable.prop('viewable', false);
                    }
                })
            }, 500);

            $(window).bind('scroll', checkViewables);
            $(window).bind('resize', checkViewables);
            window.onbeforeunload = function() {
                isUnload = true;
                syncCheckViewables();
            };

            JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, checkViewables);
            var checkInterval = setInterval(checkViewables, 1000);

            return {
                dwell: function ($element, callback)
                {
                    $element.bind('mouseenter', callback);
                },

                viewable: function ($element, callback)
                {
                    viewables.push($element);

                    $element.bind('viewable', callback);

                    checkViewables();
                },

                selection: function ($element)
                {

                },

                mousedown: function ($element, callback)
                {
                    $element.bind('mousedown', callback);
                }
            }
        });