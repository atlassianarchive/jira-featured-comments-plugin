define('featuredcomments/storage', ['jquery'], function ($)
{
    var bulk = [];
    var bulkTimeout;

    function flushBulk(sync)
    {
        if(bulk.length == 0) {
            return;
        }

        $.ajax({
            type: "POST",
            url: AJS.contextPath() + "/rest/featuredcomments/1.0/collect/bulk",
            data: JSON.stringify(bulk.splice(0, Number.MAX_VALUE)),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: !!!sync
        });
    }

    $(window).unload(function ()
    {
        if (bulkTimeout)
        {
            clearTimeout(bulkTimeout);
        }

        flushBulk(true);
    });

    return {
        pushEvent: function (commentId, eventClass, duration)
        {

            bulk.push({
                name: eventClass,
                commentId: commentId,
                duration: duration
            });

            if (bulkTimeout)
            {
                clearTimeout(bulkTimeout);
            }

            bulkTimeout = setTimeout(function ()
            {
                bulkTimeout = null;
                flushBulk();
            }, 60000);
        },

        getScore: function (commentId)
        {
            var deferred = new $.Deferred();

            $.get(AJS.contextPath() + "/rest/featuredcomments/1.0/score", {
                commentId: commentId
            }).then(function (response)
            {
                deferred.resolve(response);
            });

            return deferred;
        },

        getTopComments: function (issueKey)
        {
            var deferred = new $.Deferred();

            $.get(AJS.contextPath() + "/rest/featuredcomments/1.0/score/bulk", {
                issueKey: issueKey
            }).then(function (response)
            {
                deferred.resolve(response)
            });

            return deferred;
        }
    }
});