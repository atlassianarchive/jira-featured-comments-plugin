define('featuredcomments/comments', ['jquery', 'featuredcomments/parallax'], function ($, parallax)
{
    return {
        highlight: function ($comment)
        {
            var $marker = $('<div class="comment-marker">').prependTo($comment);
            var $container = $(window)
            parallax($marker, $container);
        },

        getId: function (comment) {
            return parseInt(comment.id.match(/^(?:comment-)(\d+)$/)[1])
        }
    }
});