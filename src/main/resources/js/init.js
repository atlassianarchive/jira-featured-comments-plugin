require(['featuredcomments/comments', 'featuredcomments/collector', 'featuredcomments/storage', 'featuredcomments/events', 'jquery'],
        function (comments, collector, storage, events, $)
        {
            var eventMap = [
                { name: 'dwell', start: 'dwell', end: 'mouseleave' },
                { name: 'viewable', start: 'viewable', end: 'outscreen' },
                { name: 'touch', start: 'mousedown', end: 'mouseup' }
            ];

            function refresh(context)
            {
                if (!JIRA.Meta.getIssueKey())
                {
                    return;
                }

                var $comments = $('.activity-comment:not(.ftc-tracked)', context);

                $comments.addClass('ftc-tracked').each(function (idx, comment)
                {
                    var $comment = $(comment);

                    eventMap.forEach(function (event)
                    {
                        events[event.start]($comment, function ()
                        {
                            var eventId = collector.eventStart(event.name, comments.getId(comment))

                            $comment.one(event.end, function ()
                            {
                                collector.eventEnd(eventId);
                            });
                        });
                    });
                });

                if ($comments.length > 0)
                {
                    storage.getTopComments(JIRA.Meta.getIssueKey()).then(function (comments)
                    {
                        if (comments.length == 0)
                        {
                            return;
                        }

                        comments = comments.filter(function (comment) { return comment.score > 0 }).sort(function (a, b) { return b.score - a.score });

                        var average = comments.reduce(function (sum, comment) { return sum + comment.score }, 0) / comments.length;
                        comments = comments.filter(function (comment) { return comment.score >= average });

                        var median = Math.floor(comments.length / 2);
                        median = comments.length % 2 ? comments[median].score : (comments[median - 1].score + comments[median].score) / 2.0;
                        comments = comments.filter(function (comment) { return comment.score >= median });

                        comments.forEach(function (comment)
                        {
                            $('<span class="comment-score">').text(AJS.I18n.getText("Important")).appendTo($('#comment-' + comment.commentId + ':not(.ftc-marked)').addClass('ftc-marked').find('.action-details'));
                        });

                        var hiddenCount = comments.filter(function (comment)
                        {
                            return $('#comment-' + comment.commentId).length == 0;
                        }).length;

                        if (hiddenCount > 0)
                        {
                            $('.collapsed-comments .show-more-comments:not(.ftc-info)').addClass('ftc-info').append(', ' + hiddenCount + ' important');
                        }
                    });
                }
            }

            [JIRA.Events.NEW_CONTENT_ADDED, JIRA.Events.ISSUE_REFRESHED, JIRA.Events.PANEL_REFRESHED].forEach(function (jiraEvent)
            {
                JIRA.bind(jiraEvent, function (e, context, reason)
                {
                    refresh(context);
                });
            });

            var state = JIRA.Issues && JIRA.Issues.URLSerializer.getStateFromURL(window.location.toString());

            // there is focusedCommentId in URL, so most likely someone is linking to this comment, which means that it is important
            if (state && state.viewIssueQuery && state.viewIssueQuery.focusedCommentId)
            {
                storage.pushEvent(state.viewIssueQuery.focusedCommentId, "link", 1);
            }

            refresh();
        });